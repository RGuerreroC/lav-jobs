<li class="nav-item dropdown">
  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Recursos
  </a>
  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
    @foreach($res as $r)
        @if($r->interno === 1)
          <a onClick="openUrl('{{ $r->url }}')" href='#' class="dropdown-item">{{ $r->nombre }}</a>
        @else
          {{ Html::link($r->url, $r->nombre, ['class' => 'dropdown-item', 'target' => '_blank']) }}
        @endif
    @endforeach
  </div>
</li>
