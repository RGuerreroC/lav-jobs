@if(Auth::check())
  <ul class="nav nav-pills flex-column card text-center">
    <li class="nav-item">
      <a class="nav-link {{ Request::is('recursos') || Request::is('recursos/*') ? 'active' : '' }}" href="/recursos">Recursos</a>
    </li>
  <li class="nav-item">
      <a class="nav-link {{ Request::is('horas') || Request::is('horas/*') ? 'active' : '' }}" href="/horas">Control Horas</a>
    </li>
  </ul>
@endif