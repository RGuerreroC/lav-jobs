<nav class="navbar fixed-bottom navbar-dark bg-info top-spacing-20">
    <small class="row col-12 justify-content-between">
        <div class="col-6">
            <a rel="license" href="http://blog.dxcodercrew.net">
                <img alt="DX Coder Crew" style="border-width:0" src="http://cdn.dxcodercrew.net/dxcc-images/favicon.png" width="32" />
            </a>
            Designed by <a class="text-light" href="http://blog.dxcodercrew.net">DX Coder Crew</a>
        </div>
        <div class="col-6 mr-auto text-right">
            <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/" class="text-light">
                License CC 4.0 International
            </a>
            <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">
                <img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/80x15.png" />
            </a>
        </div>
    </small>
</nav>