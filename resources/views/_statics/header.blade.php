<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Jobs RGC Intranet">
    <!--meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0"-->
    <title>@yield('title') | Jobs RGC Intranet</title>

    <!-- Bootstrap CSS -->
    {{ Html::style('http://cdn.dxcodercrew.net/bootstrap/dist/css/bootstrap.min.css') }}
    {{ Html::style('http://cdn.dxcodercrew.net/@fortawesome/fontawesome-free/css/all.css') }}
    {{ Html::style(asset('css/styles.css')) }}
    @yield('stylesheets')
</head>
