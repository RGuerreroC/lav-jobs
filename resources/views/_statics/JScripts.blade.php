{{ Html::script('http://cdn.dxcodercrew.net/jquery/dist/jquery.min.js') }}
{{ Html::script('http://cdn.dxcodercrew.net/popper.js/dist/umd/popper.js') }}
{{ Html::script('http://cdn.dxcodercrew.net/bootstrap/dist/js/bootstrap.js') }}

<script type="text/javascript">
$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('.dropdown-toggle').dropdown();
    $("#message-alert-auto").fadeTo(2000, 500).slideUp(500, function(){
        $("#message-alert-auto").slideUp(750);
    });
})
</script>
