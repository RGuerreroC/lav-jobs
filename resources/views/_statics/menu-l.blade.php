@if(Auth::check())
  <ul class="nav nav-pills flex-column card text-center">
    <li class="nav-item">
      <a class="nav-link {{ Request::is('tareas') || Request::is('tareas/*') ? 'active' : '' }}" href="/tareas">Tareas <small>(Programación)</small></a>
    </li>
    <li class="nav-item">
      <a class="nav-link {{ Request::is('sys_tareas') || Request::is('sys_tareas/*') ? 'active' : '' }}" href="/sys_tareas">Tareas <small>(Sistemas)</small></a>
    </li>
  </ul>
@endif