<nav class="navbar navbar-expand-lg sticky-top navbar-dark bg-info">
    <nav class="navbar">
        <a class="navbar-brand" href="#">
            <img src="{{ asset('logos/favicon.png') }}" height="50" alt=""> RGC Intranet
        </a>
    </nav>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
                <a class="nav-link" href="/"><i class="fa fa-home fa-lg"></i>Inicio</a>
            </li>
         @if(Auth::check())
          @include('../_statics/resources-list')
          <li class="nav-item {{ Request::is('/logoff') ? 'active' : '' }}">
            <a class="nav-link" href="/logoff"><i class="fa fa-power-off fa-lg"></i> Desconectar</a>
          </li>
        @else
          <li class="nav-item {{ Request::is('/login') ? 'active' : '' }}">
            <a class="nav-link" href="/login"><i class="fa fa-user-o fa-lg"></i> Conectar</a>
          </li>
        @endif
        </ul>
    </div>
</nav>
