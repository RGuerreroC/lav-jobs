<!doctype html>
<html lang="{{ app()->getLocale() }}">
  @include('_statics.header')
  <body>
    @if(Auth::check())
		@yield('content')
	@endif
    @yield('scripts')
  </body>
</html>