<!doctype html>
<html lang="{{ app()->getLocale() }}">
  @include('_statics.header')
  <body>
    @include('_statics.navbar')

    @include('_statics.msg')
    <div class="container">
      @yield('jumbotron')
    </div>
    <div class="container{{ Auth::check() ? '-fluid' : ' ' }} bottom-spacing-50">
      <div class="row justify-content-center">
        @if(Auth::check())
          <div class="col-2 mr-auto">
            @include('../_statics/menu-l')
          </div>
        @endif
        <div class="col{{ Auth::check() ? ' row' : '-10' }}">
          @yield('content')
        </div>
        @if(Auth::check())
        <div class="col-2 ml-auto">
          @include('../_statics/menu-r')
        </div>
        @endif
      </div>
    </div>
    @include('_statics.footer')
    @include('_statics.JScripts')
    @yield('scripts')
  </body>
</html>