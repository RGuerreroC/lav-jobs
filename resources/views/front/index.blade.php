@extends('_templates.main')

@section('title', 'Principal')

@section('stylesheets')
    {{ Html::style(asset('css/index.css')) }}
@endsection

@section('jumbotron')
  <div class="top-spacing-20">
    
  </div>
@endsection

@section('jumbotron_no')
  <div class="jumbotron jumbotron-fluid top-spacing-20">
      <div class="container">
          <p class="lead">

          </p>
      </div>
  </div>
@endsection

@section('content')
  
  <div class="card-columns">
    @foreach($res as $i => $resource)
      <div class="card">
        <img class="card-img mx-auto d-block top-spacing-10" src="{{ $resource->icono == 'default' ? asset('logos/favicon.png') : asset('images/'.$resource->icono) }}" alt="{{ $resource->nombre }}" alt="Card image cap">
        <div class="card-body">
          <!--h5 class="card-title">{{ $resource->nombre }}</h5-->
          <!--p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p-->
        </div>
        @if(Auth::check())
          <div class="card-footer">
          @if($resource->interno === 1)            
            <a onClick="openUrl('{{ $resource->url }}')" href='#' class="btn btn-outline-secondary btn-block top-spacing-10">{{ $resource->nombre }}</a>
          @else
            {{ Html::link($resource->url, $resource->nombre, ['class' => 'btn btn-outline-secondary btn-block top-spacing-10', 'target' => '_blank']) }}
          @endif
          </div>
        @endif
      </div>
    @endforeach
</div>
@endsection

@section('content_old')
  <div class="col-12 row justify-content-center">
    <div class="col-8">
      <div class="card-columns"> 
        @foreach($res as $i => $resource)
        <div class="card bg-light row col">
          <h5 class="card-title">
            {{ $resource->nombre }}
          </h5>
          <img class="card-img d-block" src="{{ $resource->icono == 'default' ? asset('logos/favicon.png') : asset('images/'.$resource->icono) }}" alt="{{ $resource->nombre }}">
          @if(Auth::check())
          <p class="card-text">
            @if($resource->interno === 1)
              <a onClick="openUrl('{{ $resource->url }}')" href='#' class="btn btn-outline-secondary btn-block top-spacing-10">Abrir</a>  
            @else
              {{ Html::link($resource->url, 'Abrir', ['class' => 'btn btn-outline-secondary btn-block top-spacing-10', 'target' => '_blank']) }}
            @endif
          </p>
          @endif
        </div>
        @endforeach
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script>
    function openUrl($url){
      window.open($url,'Menu3','toolbar=0, menubar=0, status=1, location=0, scrollbars=1, resizable=1, width=' + window.screen.width + ' , height=' + window.screen.height);
    }
  </script>
@endsection