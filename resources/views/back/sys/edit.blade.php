@extends('_templates.main')

@section('title', 'Tareas')

@section('stylesheets')
    {{ Html::style(asset('css/tareas/index.css')) }}
    {{ Html::style('http://cdn.dxcodercrew.net/select2/dist/css/select2.css') }}
    <script src="http://cdn.dxcodercrew.net/tinymce/tinymce.min.js"></script>

    <script type="text/javascript">
    tinymce.init({
        selector: 'textarea'
        ,language: 'es'
        ,plugins: [
            "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak code"
        ]
        ,height: 200
        ,max_height: 200
        ,min_height: 200
        ,branding: false
    });
      
    function delProjName(ele){
        ele.removeAttribute('readonly');
        ele.addEventListener('blur', function(e,ele){
          var newName = $("#project_name").val();
          var taskId = $("#task_id").val();
        
          $.ajax({
            type: 'POST',
            url: '/titleChangeSys',
            data: { tarea:taskId, projectName:newName, _token: "{{ csrf_token() }}"},
            success: function (data) {
              location.reload();
            },
            error: function(data){
              alert(JSON.stringify(data));
            }
          });
      });
    }
      
    function updateStatPost(){
      var taskId = $("#task_id").val();
      var estatCod = $("#estat").val();
      
      $.ajax({
        type: 'POST',
        url: '/estatUpdateSys',
        data: { 
          tasca: taskId, 
          estat: estatCod, 
          _token: "{{ csrf_token() }}"
        },
        success: function (data) {
          location.reload();
        },
        error: function(data){
          alert(JSON.stringify(data));
        }
      });
    }
    </script>
@endsection

@section('jumbotron')
    @if($t->estat == 0)
      <div class="jumbotron jumbotron top-spacing-20 bg-light">
    @elseif($t->estat == 1)
      <div class="jumbotron jumbotron top-spacing-20 bg-warning">
    @elseif($t->estat == 2)
      <div class="jumbotron jumbotron top-spacing-20 bg-success">
    @elseif($t->estat == 3)
      <div class="jumbotron jumbotron top-spacing-20 bg-danger">
    @else
      <div class="jumbotron jumbotron top-spacing-20 bg-info">
    @endif
    <div class="container">
      <h1 class="display-5 text-center">
       Editar tarea
      </h1>
    </div>
  </div>
@endsection

@section('content')
<div class="col-12 row justify-content-between">
  <div class="col-12 bottom-spacing-10">
    <a href="/sys_tareas/{{ $t->id }}" class="btn btn-sm circle-sm btn-secondary col-1"><span class="fa fa-arrow-left fa-sm"></span></a>
  </div>
  <div class="col-10 row justify-content-start">
    {{ Form::model($t, ['route' =>['sys_tareas.update', $t->id],'files' => 'false', 'method' => 'PUT', 'class' => 'col-12']) }}
      {{ csrf_field() }}
      @if($t->estat == 0)
        <div class="card border-light">
      @elseif($t->estat == 1)
        <div class="card border-warning">
      @elseif($t->estat == 2)
        <div class="card border-success">
      @elseif($t->estat == 3)
        <div class="card border-danger">
      @else
        <div class="card border-info">
      @endif
        <div class="card-header">
          <h4 class="card-title">
            <span class="btn btn-success circle fa fa-pencil ml-auto"></span>
          </h4>
          <h6 class="card-subtitle mb-2 text-muted"></h6>
        </div>
        <div class="card-body">
          <div class="card-text row">
            <div class="form-group col-12">
              {{ Form::hidden('task_id', $t->id, ['id' => 'task_id']) }}
              {{ Form::label('project_name', 'Nombre Proyecto') }}
              @if($t->project_name != '' || $t->project_name != null)
                {{ Form::text('project_name', null, ['class' => 'form-control','readonly', 'ondblclick' => 'delProjName(this)']) }}
              @else
                {{ Form::text('project_name', null, ['class' => 'form-control', 'onclick' => 'delProjName(this)']) }}
              @endif
            </div>
            <div class="form-group col-6">
              {{ Form::label('glpi_id', 'GLPI') }}
              {{ Form::number('glpi_id', null, ['class' => 'form-control']) }}  
            </div>
            <div class="form-group col-6">
              {{ Form::label('estat', 'Estado') }}
              {{ Form::select('estat', [null => 'Selecciona', '0' => 'Nuevo/En espera', '1' => 'En curso', '2' => 'Finalizado', '3' => 'Cancelado'], null, ['class' => 'form-control custom-select', 'onchange' => 'updateStatPost()']) }} 
            </div>
            <div class="form-group col-6">
              {{ Form::label('start_date', 'Fecha de inicio') }}
              {{ Form::date('start_date', null, ['class' => 'form-control col-md-12']) }}  
            </div>
            <div class="form-group col-6">
              {{ Form::label('finish_date', 'Fecha fin') }}
              {{ Form::date('finish_date', null, ['class' => 'form-control col-md-12']) }}  
            </div>
            <div class="form-group col-12">
              {{ Form::label('desc','Descripcion') }}
              {{ Form::textarea('desc', null,['class' => 'form-control']) }}
            </div>
          </div>
        </div>
        <div class="card-footer">
          {{ Form::submit('Guardar', ['class' => 'btn btn-outline-dark btn-block']) }}
        </div>
      </div>
    {{ Form::close() }}
  </div>
  <div class="col ml-auto">
    <div class="card">
      <div class="card-body">
        <h5>Información</h5>
        <ul class="nav flex-column text-left">
          <li class="nav-item">
            <span class="fa fa-calendar"></span> Creado
          </li>
          <li class="nav-item">
            {{ Date::parse($t->created_at)->format('d/m/Y') }}
            {{ Date::parse($t->created_at)->format('H:i') }}
          </li>
          <hr />
          <li class="nav-item">
            <span class="fa fa-calendar"></span> Modificado
          </li>
          <li class="nav-item">
            {{ Date::parse($t->updated_at)->format('d/m/Y') }}
            {{ Date::parse($t->updated_at)->format('H:i') }}
          </li>
        </ul>
      </div>
      {{ Form::open(['route' => ['sys_tareas.destroy', $t->id], 'method' => 'DELETE']) }}
        {{ Form::submit('Eliminar', ['class' => 'btn btn-outline-danger btn-block']) }}
      {{ Form::close() }}
    </div>
      
  </div>
</div>
@endsection