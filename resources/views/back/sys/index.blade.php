@extends('_templates.main')

@section('title', 'Tareas de sistemas')

@section('stylesheets')
    {{ Html::style(asset('css/tareas/index.css')) }}
    {{ Html::style('http://cdn.dxcodercrew.net/select2/dist/css/select2.css') }}
    <script src="http://cdn.dxcodercrew.net/tinymce/tinymce.min.js"></script>

    <script type="text/javascript">
    tinymce.init({
        selector: 'textarea'
        ,language: 'es'
        ,plugins: [
            "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak code"
        ]
        ,height: 200
        ,max_height: 200
        ,min_height: 200
        ,branding: false
    });
    </script>
@endsection

@section('jumbotron')
  <div class="jumbotron jumbotron top-spacing-20">
    <div class="container">
      <h1 class="display-5 text-center">
        Tareas de sistemas
      </h1>
    </div>
  </div>
  <!-- Modal -->
  <div class="modal fade" id="tareaModal" tabindex="-1" role="dialog" aria-labelledby="tareaModalLabel" aria-hidden="true">
    @include('back/sys/create')
  </div>
@endsection

@section('content')
  <div class="col-12 row justify-content-center">
    <div class="col-12 row justify-content-between">
      <div class="btn-group-sm ml-auto" role="group">
        <button type="button" class="btn btn-default circle-sm" data-toggle="modal" data-target="#tareaModal"><span class="fa fa-plus fa-sm"></span></button>
        <button id="btnGroupDrop1" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Mostrar
        </button>
        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
          <a class="dropdown-item" href="/sys_tareas"><span class="badge badge-pill badge-primary">{{ ($tAll > 9 ? '' : '0') }}{{ $tAll }}</span>Todos</a>
          <a class="dropdown-item" href="/sys_tareas/estado/0"><span class="badge badge-pill badge-secondary">{{ ($tNew > 9 ? '' : '0') }}{{ $tNew }}</span> Nuevos/En espera</a>
          <a class="dropdown-item" href="/sys_tareas/estado/1"><span class="badge badge-pill badge-warning">{{ ($tCur > 9 ? '' : '0') }}{{ $tCur }}</span> En curso</a>
          <a class="dropdown-item" href="/sys_tareas/estado/2"><span class="badge badge-pill badge-success">{{ ($tOk > 9 ? '' : '0') }}{{ $tOk }}</span> Finalizados</a>
          <a class="dropdown-item" href="/sys_tareas/estado/3"><span class="badge badge-pill badge-danger">{{ ($tKo > 9 ? '' : '0') }}{{ $tKo }}</span> Cancelados</a>
        </div>
      </div>
    </div>
    <div class="row col-12 justify-content-center">
      {!! $ts->render('vendor.pagination.bootstrap-4') !!}
    </div>
    <div class="col-6">
      <div class="list-group col">
        @foreach($ts as $i => $t)
        <a href="{{ route('sys_tareas.show',$t->id)}}" class="list-group-item list-group-item-action list-group-item-{{ $t->estat === null || $t->estat === 0 ? 'light' : ($t->estat === 1 ? 'warning' : ($t->estat ===2 ? 'success' : 'danger')) }}">
          <div class="row justify-content-between">
            <div class="col">
              @if($t->project_name != '' || $t->project_name != null)
                Proyecto: {{ $t->project_name }}
              @else
                @if($t->glpi_id === null)
                  Tarea sin numero de GLPI o Redmine
                @else
                  [GLPI: #{{ $t->glpi_id }}]
                @endif
              @endif
            </div>
            <div class="col-1 text-right">
              <i class="fa fa-arrow-right"></i>
            </div>
          </div>
        </a>
        @endforeach
      </div>
    </div>
    <div class="row col-12 justify-content-center top-spacing-50">
        {!! $ts->render('vendor.pagination.bootstrap-4') !!}
    </div>
  </div>
@endsection