<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="tareaModalLabel">Nueva tarea</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
      <div class="modal-body">
      {!! Form::open(['route' => 'sys_tareas.store']) !!}
        <div class="form-inline">
          <div class="form-group">
            {{ Form::label('glpi_id', 'GLPI') }}
            {{ Form::number('glpi_id', null, ['class' => 'form-control col-md-12']) }}  
          </div>
        </div>
        <div class="form-inline">
          <div class="form-group">
            {{ Form::label('start_date', 'Fecha de inicio') }}
            {{ Form::date('start_date', null, ['class' => 'form-control col-md-12']) }}  
          </div>
          <div class="form-group left-spacing-50">
            {{ Form::label('finish_date', 'Fecha fin') }}
            {{ Form::date('finish_date', null, ['class' => 'form-control col-md-12']) }}  
          </div>
        </div>
        <div class="form-group">
          {{ Form::label('project_name', 'Nombre Proyecto') }}
          {{ Form::text('project_name', null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group">
          {{ Form::label('desc','Descripcion') }}
          {{ Form::textarea('desc', null,['class' => 'form-control']) }}
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
        {{ Form::submit('Guardar', ['class' => 'btn btn-outline-dark btn-block']) }}  
      </div>
      {{ Form::close() }}
    </div>
  </div>
</div>