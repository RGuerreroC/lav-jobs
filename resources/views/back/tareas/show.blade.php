@extends('_templates.main')

@section('title', 'Tareas')

@section('stylesheets')
    {{ Html::style(asset('css/tareas/index.css')) }}
    {{ Html::style('http://cdn.dxcodercrew.net/select2/dist/css/select2.css') }}
    <script src="http://cdn.dxcodercrew.net/tinymce/tinymce.min.js"></script>

    <script type="text/javascript">
    tinymce.init({
        selector: 'textarea'
        ,language: 'es'
        ,plugins: [
            "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak code"
        ]
        ,height: 200
        ,max_height: 200
        ,min_height: 200
        ,branding: false
    });
    </script>
    
@endsection

@section('jumbotron')
    @if($t->estat == 0)
      <div class="jumbotron jumbotron top-spacing-20 bg-light">
    @elseif($t->estat == 1)
      <div class="jumbotron jumbotron top-spacing-20 bg-warning">
    @elseif($t->estat == 2)
      <div class="jumbotron jumbotron top-spacing-20 bg-success">
    @elseif($t->estat == 3)
      <div class="jumbotron jumbotron top-spacing-20 bg-danger">
    @else
      <div class="jumbotron jumbotron top-spacing-20 bg-info">
    @endif
    <div class="container">
      <h1 class="display-5 text-center">
       Ver tarea
      </h1>
    </div>
  </div>
@endsection

@section('content')
<div class="col-12 row justify-content-between">
  <div class="col-10 row justify-content-between">
    <div class="col-12 bottom-spacing-10">
      <a href="/tareas/estado/1" class="btn btn-sm circle-sm btn-secondary col-1"><span class="fa fa-arrow-left fa-sm"></span></a>
    </div>
    <div class="col-8"> 
        @if($t->estat == 0)
          <div class="card border-light">
        @elseif($t->estat == 1)
          <div class="card border-warning">
        @elseif($t->estat == 2)
          <div class="card border-success">
        @elseif($t->estat == 3)
          <div class="card border-danger">
        @else
          <div class="card border-info">
        @endif
          <div class="card-header">
            <h4 class="card-title"><span class="btn btn-secondary circle fa fa-info"></span> Descripción</h4>
            @if($t->project_name != '' || $t->project_name != null)
            <h5 class="card-subtitule mb-2">
              {{ $t->project_name }}
            </h5>
            @endif
            <h6 class="card-subtitle mb-2 text-muted">Estado: {{ $t->estat === null || $t->estat === 0 ? 'Nueva' : ($t->estat === 1 ? 'En curso' : ($t->estat === 2 ? 'Finalizado' : 'Cancelado')) }}</h6>
          </div>
          <div class="card-body">
            <p class="card-text">{!! $t->desc !!}</p>
          </div>
          <div class="card-footer">
            <div class="col-12 row justify-content-between">
              <div class="col-8">
                <h3>Cambios</h3>
              </div>
              <div class="col-4 text-right">
                <!-- Button trigger modal -->
                @if($t->estat !== 2)
                <button type="button" class="btn btn-light btn-sm circle" data-toggle="modal" data-target="#revisionesModal"><span class="fa fa-plus fa-lg"></span></button>
                @endif
                <!-- Modal -->
                <div class="modal fade" id="revisionesModal" tabindex="-1" role="dialog" aria-labelledby="revisionesModal" aria-hidden="true">
                  @include('back/rev/create')
                </div>
              </div>
            </div>
            <div class="row justify-content-center">
              <div class="list-group  col-8">
                @foreach($t->part_rev($t->id) as $i => $rev)
                  <a href="{{ route('revisiones.show', $rev->id) }}" class="list-group-item list-group-item-action {{ $i%2 == 0 ? 'list-group-item-light' : 'list-group-item-secondary'}}">
                    <div class="d-flex w-100 justify-content-between">
                      <h5 class="mb-1">Revisión {{ $rev->num_rev }}</h5>
                      <small>{{$rev->hash !== null ? '(Hash: '.$rev->hash : '(Sin hash' }})</small>
                    </div>
                    <div class="row justify-content-between">
                      <div class="col">
                        
                      </div>
                      <div class="col-1 text-right">
                        <i class="fa fa-arrow-right"></i>
                      </div>
                    </div>
                  </a>
                @endforeach
              </div>
            </div>
          </div>
        </div>
    </div>
    <div class="col-4">
      @if($t->estat == 0)
        <div class="card border-light">
      @elseif($t->estat == 1)
        <div class="card border-warning">
      @elseif($t->estat == 2)
        <div class="card border-success">
      @elseif($t->estat == 3)
        <div class="card border-danger">
      @else
        <div class="card border-info">
      @endif
        <div class="card-header">
          <h4 class="card-title"><span class="btn btn-secondary circle fa fa-link"></span> Enlaces</h4>
          <h6 class="card-subtitle mb-2 text-muted">
          </h6>
        </div>
        <div class="card-body">
          <p class="card-text">
            <div class="">
              {{ Form::label('glpi_id', 'GLPI:') }}
              @if($t->glpi_id === null)
                {{ Form::text('glpi_id', 'Sin numero de GLPI', ['class' => 'form-control-plaintext', 'readonly']) }}
              @else
                {{ Html::link('http://glpi.mgc.es/front/ticket.form.php?id='.$t->glpi_id, $t->glpi_id, ['class' => 'btn btn-link', 'target' => '_blank']) }}
              @endif
            </div>
            <div class="">
              {{ Form::label('redmine_id', 'Redmine:') }}
              @if($t->redmine_id === null)
                {{ Form::text('redmine_id', 'Sin numero de GLPI', ['class' => 'form-control-plaintext', 'readonly']) }}
              @else
                {{ Html::link('http://svn-server.mgc.es/redmine/issues/'.$t->redmine_id, $t->redmine_id, ['class' => 'btn btn-link', 'target' => '_blank']) }}
              @endif
            </div>
            <div class="">
              {{ Form::label('redmine_t', 'Tipo:') }}
              {{ Form::text('redmine_t', $t->redmine_t === '1' ? 'Tasca' : ($t->redmine_t === '2' ? 'Error' : 'No especificado' ), ['class' => 'form-control-plaintext', 'readonly']) }}
            </div>
          </p>
        </div>
        <div class="card-header">
          <h4 class="card-title"><span class="btn btn-secondary circle fa fa-database"></span> Repositori</h4>
          <h6 class="card-subtitle mb-2 text-muted"> </h6>
        </div>
        <div class="card-body">
          <p class="card-text">
            {{ $t->repo }}
          </p>
        </div>
          
        <div class="card-footer text-muted">
          @if($t->project_name != '' || $t->project_name != null)
                [Projecte: {{ $t->project_name }}]
          @endif
          @if($t->redmine_id === null && $t->glpi_id === null)
              Sin información
          @else
            @if($t->redmine_id !== null)
              @if($t->glpi_id !== null)
                @if($t->redmine_t !== '0')
                  {{ $t->redmine_t === '1' ? '[Tasca' : '[Error' }}:
                @else
                  [Redmine: 
                @endif
                #{{ $t->redmine_id }}][GLPI: #{{ $t->glpi_id }}]
              @else
                @if($t->redmine_t !== '0')
                  {{ $t->redmine_t === '1' ? '[Tasca' : '[Error' }}:
                @endif
                  {{ $t->redmine_id }}]
              @endif
            @else
              [GLPI: #{{ $t->glpi_id }}]
            @endif
          @endif
        </div>
      </div>
    </div>
  </div>

  <div class="col ml-auto">
    <div class="card">
      <div class="card-body">
        <h5>Información</h5>
        <ul class="nav flex-column text-left">
          <li class="nav-item">
            <span class="fa fa-calendar"></span> Creado
          </li>
          <li class="nav-item">
            {{ Date::parse($t->created_at)->format('d/m/Y') }}
            {{ Date::parse($t->created_at)->format('H:i') }}
          </li>
          <hr />
          <li class="nav-item">
            <span class="fa fa-calendar"></span> Modificado
          </li>
          <li class="nav-item">
            {{ Date::parse($t->updated_at)->format('d/m/Y') }}
            {{ Date::parse($t->updated_at)->format('H:i') }}
          </li>
        </ul>
      </div>
      <a href="{{ route('tareas.edit', $t->id) }}" class="btn btn-outline-primary btn-block"><span class="fa fa-pencil"></span> Editar</a>
    </div>
      
  </div>
</div>
@endsection