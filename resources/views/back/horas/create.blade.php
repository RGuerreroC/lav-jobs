<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="tareaModalLabel">Añadir Horas</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
      <div class="modal-body">
      {!! Form::open(['route' => 'horas.store']) !!}
		<div class="form-inline">
			<div class="form-group">
          		{{ Form::label('start_date', 'Entrada:') }}
				<input type="datetime-local" id="start_date" name="start_date" class="form-control-plaintext left-spacing-10" value="{{ \Jenssegers\Date\Date::now() }}" />
          	</div>
			<div class="form-group left-spacing-30">
            	{{ Form::label('finish_date', 'Salida:') }}
				<input type="datetime-local" id="finish_date" name="finish_date" class="form-control-plaintext left-spacing-10" />
          	</div>
        </div>
		<div class="form-inline">
			<div class="form-group">
				{{ Form::label('dinner', 'Comida:') }}
				{{ Form::time('dinner', '00:00', ['class' => 'form-control-plaintext left-spacing-10']) }}
			</div>
			<div class="form-group left-spacing-30">
				{{ Form::checkbox('free_day', null, false) }}
				{{ Form::label('free_day', 'Festivo/Vacaciones', ['class' => 'left-spacing-10']) }}
			</div>
		</div>
		<div class="from-group">
			{{ Form::label('descripcion','Descripcion') }}
          	{{ Form::textarea('descripcion', null,['class' => 'form-control']) }}
		</div>
     	<div class="modal-footer">
			<button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
			{{ Form::submit('Guardar', ['class' => 'btn btn-outline-dark btn-block']) }}
		  </div>
      {{ Form::close() }}
    </div>
  </div>
</div>
