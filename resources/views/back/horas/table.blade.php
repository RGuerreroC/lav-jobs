<div class="row col-12 bg-secondary text-white border border-dark">
	<div class="col-1 text-center">
		Dia
	</div>
	<div class="col-1 text-center">
		Lugar
	</div>
	<div class="col-3 text-center">
		Descripcion
	</div>
	<div class="col-1 text-center">
		H.Totales
	</div>
	<div class="col-2 text-center">
		H.Trabajadas
	</div>
	<div class="col-1 text-center">
		H.Ausencia
	</div>
	<div class="col-2 text-right">
		Motivo/observaciones
	</div>
</div>
@foreach($whs as $wh)
<a href="{{ route('horas.edit', $wh->id) }}" class="text-dark">
	<div class="row col-12 bg-{{ (Date::parse($wh->start_date)->format('l') != 'sábado' and Date::parse($wh->start_date)->format('l') != 'domingo') ? (($wh->descripcion != null or $wh->free_day == 1) ? 'info-pers' : 'light') : 'info' }} border border-dark">
		<div class="col-1 text-center">
			{{ Date::parse($wh->start_date)->format('d') }}
		</div>
		<div class="col-1 text-center">
			ACTIVA
		</div>
		<div class="col-3 text-center border-dark">
			DESARROLLO APLICACIONES
		</div>
		@if(Date::parse($wh->start_date)->format('F') == 'julio' or Date::parse($wh->start_date)->format('F') == 'agosto')
			@if((Date::parse($wh->start_date)->format('l') != 'sábado' and Date::parse($wh->start_date)->format('l') != 'domingo') and (!$wh->free_day))
				<div class="col-1 text-center">7.00</div>
			@else
				<div class="col-1 text-center">0.00</div>
			@endif
		@else
			@if((Date::parse($wh->start_date)->format('l') != 'sábado' and Date::parse($wh->start_date)->format('l') != 'domingo') and (!$wh->free_day))
				@if(Date::parse($wh->start_date)->format('l') == 'viernes')
					<div class="col-1 text-center">6.00</div>
				@else
					<div class="col-1 text-center">8.50</div>
				@endif
			@else
				<div class="col-1 text-center">0.00</div>
			@endif
		@endif
		<div class="col-2 text-center">{{ number_format($wh->total_hours, 2) }}</div>
		@if($wh->descripcion == null)
			@if(Date::parse($wh->start_date)->format('F') == 'julio' or Date::parse($wh->start_date)->format('F') == 'agosto')
				@if((Date::parse($wh->start_date)->format('l') != 'sábado' and Date::parse($wh->start_date)->format('l') != 'domingo') and (!$wh->free_day) )
					<div class="col-1 text-center">{{ number_format(($wh->total_hours + -7.00), 2) }}</div>
				@else
					<div class="col-1 text-center">0.00</div>
				@endif
			@else
				@if((Date::parse($wh->start_date)->format('l') != 'sábado' and Date::parse($wh->start_date)->format('l') != 'domingo') and (!$wh->free_day))
					@if(Date::parse($wh->start_date)->format('l') == 'viernes')
						<div class="col-1 text-center">{{ number_format(($wh->total_hours + -6.00), 2) }}</div>
					@else
						<div class="col-1 text-center">{{ number_format(($wh->total_hours + -8.50), 2) }}</div>
					@endif
				@else
					<div class="col-1 text-center">0.00</div>
				@endif
			@endif
		@else
			<div class="col-1 text-center">0.00</div>
		@endif
		<div class="col-2 text-right">{{ strip_tags($wh->descripcion) }}</div>
	</div>
</a>
@endforeach
<div class="row col-12 bg-secondary text-white border border-dark">
	<div class="col-5 text-center">
	</div>
	<div class="col-1 text-center">
		Teoricas
	</div>
	<div class="col-2 text-center">
		Reales
	</div>
	<div class="col-1 text-center">
		Recuperar
	</div>
</div>
<div class="row col-12 bg-warning border border-dark">
	<div class="col-5 text-right">
		TOTAL:
	</div>
	@php
		$tot = 0;
		$trab = 0;
		$rec = 0;
		foreach($whs as $i=>$wh){
			$trab = $trab+$wh->total_hours;


			if(Date::parse($wh->start_date)->format('F') == 'julio' or Date::parse($wh->start_date)->format('F') == 'agosto'){
				if((Date::parse($wh->start_date)->format('l') != 'sábado' and Date::parse($wh->start_date)->format('l') != 'domingo') and (!$wh->free_day)){
					$tot = $tot+7.00;
					$rec = ($wh->descripcion == null) ? $rec+($wh->total_hours + -7.00) : $rec;
				}else{
					$tot = $tot+0;
				}
			}else{
				if((Date::parse($wh->start_date)->format('l') != 'sábado' and Date::parse($wh->start_date)->format('l') != 'domingo') and (!$wh->free_day)){
					if(Date::parse($wh->start_date)->format('l') == 'viernes'){
						$tot = $tot+6;
						$rec = ($wh->descripcion == null) ? $rec+($wh->total_hours + -6.00) : $rec;
					}else{
						$tot = $tot+8.50;
						$rec = ($wh->descripcion == null) ? $rec+($wh->total_hours + -8.50) : $rec;
					}
				}else{
					$tot = $tot+0;
				}
			}
		}
	@endphp
	<div class="col-1 text-center">{{ number_format($tot, 2) }}</div>
	<div class="col-2 text-center">{{ number_format($trab, 2) }}</div>
	<div class="col-1 text-center">{{ number_format($rec, 2) }}</div>
</div>
