@extends('_templates.main')

@section('title', 'Horas')

@section('stylesheets')
    {{ Html::style(asset('css/tareas/index.css')) }}
    {{ Html::style('http://cdn.dxcodercrew.net/select2/dist/css/select2.css') }}
    <script src="http://cdn.dxcodercrew.net/tinymce/tinymce.min.js"></script>

    <script type="text/javascript">
    tinymce.init({
        selector: 'textarea'
        ,language: 'es'
        ,plugins: [
            "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak code"
        ]
        ,height: 200
        ,max_height: 200
        ,min_height: 200
        ,branding: false
    });
    </script>
@endsection

@section('jumbotron')
<div class="jumbotron jumbotron top-spacing-20">
	<div class="container">
		<h1 class="display-5 text-center">
			Horas Trabajadas
		</h1>
		<hr />

		<p class="lead row justify-content-between">
			<a href="{{ URL::previous() }}" class="btn btn-secondary col-1"><span class="fa fa-arrow-left fa-lg"></span></a>
			<!-- Button trigger modal -->
			<button type="button" class="btn btn-secondary col-1" data-toggle="modal" data-target="#horasModal"><span class="fa fa-plus fa-lg"></span></button>
		</p>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="horasModal" tabindex="-1" role="dialog" aria-labelledby="horasModalLabel" aria-hidden="true">
	@include('back/horas/create')
</div>
@endsection

@section('content')
<div class="col-12 row justify-content-center">
	<div class="col-10 row">
		{{ Form::model($wh, ['route' =>['horas.update', $wh->id],'files' => 'false', 'method' => 'PUT', 'class' => 'col-12']) }}
		<div class="form-inline">
			<div class="form-group">
          		{{ Form::label('start_date', 'Entrada:') }}
				<input type="datetime-local" id="start_date" name="start_date" class="form-control-plaintext" value="{{ \Jenssegers\Date\Date::Parse($wh->start_date)->format('Y-m-d\TH:i') }}" />
          	</div>
			<div class="form-group left-spacing-30">
            	{{ Form::label('finish_date', 'Salida:') }}
				<input type="datetime-local" id="finish_date" name="finish_date" class="form-control-plaintext" value="{{ \Jenssegers\Date\Date::Parse($wh->finish_date)->format('Y-m-d\TH:i') }}" />
          	</div>
			<div class="form-group">
				{{ Form::label('dinner', 'Comida:') }}
				{{ Form::time('dinner', null, ['class' => 'form-control-plaintext']) }}
			</div>
			<div class="form-group">
				{{ Form::checkbox('free_day', null, null) }}
				{{ Form::label('free_day', ' Festivo/Vacaciones') }}
			</div>
		</div>
		<div class="form-inline">
			{{ Form::label('descripcion','Descripcion') }}
          	{{ Form::textarea('descripcion', null,['class' => 'form-control']) }}
		</div>
		<div class="form-inline">
			{{ Form::submit('Guardar', ['class' => 'btn btn-outline-dark col-11']) }}
			{{ Form::button('Eliminar', ['class' => 'btn btn-danger col-1', 'data-toggle' => 'modal', 'data-target'=>'#delHorasModal']) }}
		</div>
		{{ Form::close() }}
	</div>
</div>
<div class="modal fade" id="delHorasModal" tabindex="-1" role="dialog" aria-labelledby="delHorasModalLabel" aria-hidden="true">
	@include('back/horas/delete')
</div>
@endsection
