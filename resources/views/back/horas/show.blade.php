@extends('_templates.main_min')

@section('title', 'Horas')

@section('content')
<div class="container-fluid">
	<div class="row col-12 justify-content-left">
		<div class="col-8">
			<div class="row col-7 justify-content-between">
				<div class="col-1">
					<b>TECNICO:</b>
				</div>
				<div class="col-5 text-right">
					Raul Guerrero Carrasco (568861)
				</div>
				<div class="col-1">
					<b>MES:</b>
				</div>
				<div class="col-1 text-capitalize">
					{{ $month }}
				</div>
			</div>
			<div class="row col-7 justify-content-between">
				<div class="col-1">
					<b>CLIENTE:</b>
				</div>
				<div class="col-5 text-right">
					ACTIVA MUTUA 2008 (9999)
				</div>
				<div class="col-1"></div>
				<div class="col-1"></div>
			</div>
			<div class="row col-7 justify-content-between">
				<div class="col-1">
					<b>PROYECTO:</b>
				</div>
				<div class="col-5 text-right">
					(99999)
				</div>
				<div class="col-1">
					<b>AÑO:</b>
				</div>
				<div class="col-1">
					{{ $year }}
				</div>
			</div>
		</div>
		<div class="col-2">
			<img src="{{ asset('logos/Tecnocom.png') }}">
		</div>
	</div>
	<div class="row col-12 top-spacing-10 left-spacing-10">
		<div class="col-10" id="topdf">
			@include('/back/horas/table')
		</div>
		<div class="col-2 col-1-offset top-spacing-50">
			<div class="card col-12 border border-dark">
			  <div class="card-body">
				<p class="card-title">Vº Bº Colaborador</p>
				<div class="card-text" style="height: 175px">
					<img class="firma-img" src="{{ asset('images/firma.png') }}">
				</div>
			  </div>
			</div>
			<div class="card col-12 border border-dark top-spacing-10">
			  <div class="card-body">
				<p class="card-title">Vº Bº Cliente</p>
			  	<p class="card-text" style="height: 65px"></p>
			  </div>
			</div>
		</div>
	</div>
</div>
@endsection
