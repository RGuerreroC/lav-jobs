@extends('_templates.main')

@section('title', 'Horas')

@section('stylesheets')
    {{ Html::style(asset('css/tareas/index.css')) }}
    {{ Html::style('http://cdn.dxcodercrew.net/select2/dist/css/select2.css') }}
    <script src="http://cdn.dxcodercrew.net/tinymce/tinymce.min.js"></script>

    <script type="text/javascript">
    tinymce.init({
        selector: 'textarea'
        ,language: 'es'
        ,plugins: [
            "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak code"
        ]
        ,height: 200
        ,max_height: 200
        ,min_height: 200
        ,branding: false
    });
    </script>
@endsection

@section('jumbotron')
<div class="jumbotron jumbotron top-spacing-20">
	<div class="container">
		<h1 class="display-5 text-center">
			Horas Trabajadas
		</h1>
		<hr />

		<div class="lead row justify-content-between">
			<a href="/horas/{{ Date::parse($date)->format('m-Y') }}" target="_blank" class="btn btn-secondary col-1"><span class="fa fa-arrow-down fa-lg"></span></a>
			<!-- Button trigger modal -->
			<!--button type="button" class="btn btn-secondary col-1" data-toggle="modal" data-target="#horasModal"><span class="fa fa-plus fa-lg"></span></button-->
      <div class="btn-group col-1" role="group">
        <a href="{{ route('horas.auto', Date::parse($date)->format('m-Y')) }}" class="btn btn-secondary"><span class="fa fa-plus fa-lg"></span></a>

        <div class="btn-group" role="group">
          <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          </button>
          <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#horasModal">Personalizado</a>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="horasModal" tabindex="-1" role="dialog" aria-labelledby="horasModalLabel" aria-hidden="true">
	@include('back/horas/create')
</div>
@endsection

@section('content')
<div class="col-12 row justify-content-center">
	<div class="col-12">
		<div class="list-group col">
			<div class="list-group-item list-group-item-dark">
				<div class="row justify-content-between">
					<div class="col-1 text-left left-spacing-10">
						<a href="/horas/{{ Date::parse($date)->sub('1 month')->format('m-Y') }}/index"><i class="fa fa-arrow-left"></i></a>
					</div>
					<div class="col text-center title-month text-capitalize">{{ $date }}</div>
					<div class="col-1 text-right right-spacing-10">
						<a href="/horas/{{ Date::parse($date)->add('1 month')->format('m-Y') }}/index"><i class="fa fa-arrow-right"></i></a>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid col-12">
			<div class="row col-12 top-spacing-10 left-spacing-10">
				<div class="col-12" id="toview">
					@include('/back/horas/table')
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
