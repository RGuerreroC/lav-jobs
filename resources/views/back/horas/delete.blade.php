<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="tareaModalLabel">Borrar Horas</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
      <div class="modal-body">
      {{ Form::open(['route' => ['horas.destroy', $wh->id], 'method' => 'DELETE']) }}
		¿Deseas borrar esta imputación?
		Esto eliminará definitivamente el registro.
     	<div class="modal-footer">
			{{ Form::submit('Borrar definitivamente', ['class' => 'btn btn-outline-danger btn-block']) }}  
		 </div>
      {{ Form::close() }}
    </div>
  </div>
</div>