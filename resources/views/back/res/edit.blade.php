@extends('_templates.main')

@section('title', 'Recursos')

@section('stylesheets')
  {{ Html::style(asset('css/index.css')) }}
@endsection

@section('jumbotron')
  <div class="jumbotron jumbotron top-spacing-20">
    <div class="container">
      <h1 class="display-5 text-center">
        Editar <em>{{ $re->nombre }}</em>
      </h1>
      <hr />
      <p class="lead row justify-content-between">
        <a href="{{ URL::previous() }}" class="btn btn-secondary col-1"><span class="fa fa-arrow-left fa-lg"></span></a>
      </p>
    </div>
  </div>
@endsection
@section('content')
  <div class="col-12 row justify-content-between">
    <div class="col-8">
      {{ Form::model($re,['route' => ['recursos.update', $re->id], 'files' => 'true', 'method' => 'PUT']) }}
      <div class="form-group">
        {{ Form::label('nombre', 'Nombre') }}
        {{ Form::text('nombre', null, ['class' => 'form-control']) }}
      </div>
      <div class="form-group">
        {{ Form::label('url', 'URL') }}
        {{ Form::text('url', null, ['class' => 'form-control']) }}
      </div>
      <div class="form-group">
        <label class="custom-control custom-checkbox">
          {{ Form::checkbox('interno', 'true',$re->interno == 0 ? true : false, ['class' => 'custom-control-input']) }}
          <span class="custom-control-indicator"></span>
          <span class="custom-control-description">Abrir en pestaña</span>
        </label>
      </div>
      <div class="form-group">
        <label class="btn btn-outline-primary btn-file col-3">
          <span class="text-center">Seleccionar imagen...</span>
          {{ Form:: file('iconImg') }}
        </label>
        <span id="filename"><i class="fa fa-file-image-o"></i> {{ $re->icono == 'default' ? 'Ninguno' : $re->icono }}</span>
        @if($re->icono != 'default')
        <div>
          <label class="custom-control custom-checkbox">
            {{ Form::checkbox('del_img', 'true', null, ['class' => 'custom-control-input']) }}
            <span class="custom-control-indicator"></span>
            <span class="custom-control-description">Eliminar imagen</span>
          </label>
        </div>
        @endif
      </div>
      <div class="form-group">
        {{ Form::submit('Guardar', ['class' => 'btn btn-outline-dark btn-block']) }}
      </div>
      {{ Form::close() }}
    </div>
    <div class="col-2 ml-auto">
      <ul class="nav flex-column text-left bottom-spacing-30">
            <h5 class="text-center">
              Información
            </h5>
            <li class="nav-item">
              <span class="fa fa-calendar"></span> Creado
            </li>
            <li class="nav-item">
              {{ Date::parse($re->created_at)->format('d/m/Y') }}
              {{ Date::parse($re->created_at)->format('H:i') }}
            </li>
            <hr />
            <li class="nav-item">
              <span class="fa fa-calendar"></span> Modificado
            </li>
            <li class="nav-item">
              {{ Date::parse($re->updated_at)->format('d/m/Y') }}
              {{ Date::parse($re->updated_at)->format('H:i') }}
            </li>
            <hr />
            {{ Form::open(['route' => ['recursos.destroy', $re->id], 'method' => 'DELETE']) }}
              {{ Form::submit('Borrar', ['class' => 'btn btn-outline-danger btn-block']) }}
            {{ Form::close() }}
          </ul>
    </div>
  </div>
@endsection

@section('scripts')
  <script>
    $(document).on('change',':file', function(){
      $(this).parent().removeClass('btn-outline-primary');
      $(this).parent().addClass('btn-outline-success');
      $imgName = $(this).val().substring($(this).val().lastIndexOf("\\") + 1, $(this).val().length);
      $(this).parent().children('span').text('Imagen cargada');
      $("#filename").html(' <i class="fa fa-file-image-o"></i> ' + $imgName);
    });
  </script>
@endsection
