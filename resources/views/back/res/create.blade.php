<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="recursoModalLabel">Nuevo recurso</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    {!! Form::open(['route' => 'recursos.store', 'files' => 'true']) !!}
    <div class="modal-body">
      <div class="form-group">
        {{ Form::label('nombre', 'Nombre') }}
        {{ Form::text('nombre', null, ['class' => 'form-control']) }}  
      </div>
      <div class="form-group">
        {{ Form::label('url', 'URL') }}
        {{ Form::text('url', null, ['class' => 'form-control']) }}
      </div>
      <div class="form-group">
        <label class="custom-control custom-checkbox">
          {{ Form::checkbox('interno', 'true', true, ['class' => 'custom-control-input']) }}
          <span class="custom-control-indicator"></span>
          <span class="custom-control-description">Abrir en pestaña</span>
        </label>
      </div>
      <div class="form-group">
        <label class="btn btn-outline-primary btn-file col-5">
          <span class="text-center">Seleccionar imagen...</span>
          {{ Form:: file('iconImg') }}
        </label>
        <span id="filename"></span>
      </div>
      <div class="form-group">
        
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
      {{ Form::submit('Guardar', ['class' => 'btn btn-outline-dark btn-block']) }}  
    </div>
    {{ Form::close() }}
  </div>
</div>  
