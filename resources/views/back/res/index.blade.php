@extends('_templates.main')

@section('title', 'Recursos')

@section('stylesheets')
    {{ Html::style(asset('css/index.css')) }}
@endsection

@section('jumbotron')
  <div class="jumbotron jumbotron top-spacing-20">
    <div class="container">
      <h1 class="display-5 text-center">
        Recursos
      </h1>
      <hr />
      <p class="lead row justify-content-between">
        <a href="{{ URL::previous() }}" class="btn btn-secondary col-1"><span class="fa fa-arrow-left fa-lg"></span></a>
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-secondary col-1" data-toggle="modal" data-target="#recursoModal"><span class="fa fa-plus fa-lg"></span></button>
      </p>
    </div>
  </div>
  <!-- Modal -->
  <div class="modal fade" id="recursoModal" tabindex="-1" role="dialog" aria-labelledby="recursoModalLabel" aria-hidden="true">
    @include('back/res/create')
  </div>
@endsection

@section('content')
  <table class="table table-sm table-hover table-bordered">
    <thead class="thead-dark">
      <tr>
        <th class="text-center">Nombre</th>
        <th class="text-center">URL</th>
        <th class="text-center">Ventana</th>
        <th class="text-center">Imagen</th>
        <th class="text-center">Acciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach($res as $i => $resource)
      <tr>
        <td>{{ $resource->nombre }}</td>
        <td>{{ Html::link($resource->url, 'Enlace', ['class' => 'btn btn-link']) }}</td>
        <td class="text-center"><span class="fa fa-{{ $resource->interno == 1 ? 'check-square-o' : 'square-o'  }}"></span> </td>
        <td class="text-center"><img src="{{ $resource->icono == 'default' ? asset('logos/favicon.png') : asset('images/'.$resource->icono) }}" alt="{{ $resource->nombre }}" style="border-radius: 50%" height="50" width="50" /></td>
        <td class="text-center"><a href="{{ route('recursos.edit', $resource->id) }}"><span class="fa fa-pencil fa-lg"></span></a></td>
      </tr>
      @endforeach
    </tbody>
  </table>
@endsection

@section('scripts')
  <script>
    $(document).on('change',':file', function(){
      $(this).parent().removeClass('btn-outline-primary');
      $(this).parent().addClass('btn-outline-success');
      $imgName = $(this).val().substring($(this).val().lastIndexOf("\\") + 1, $(this).val().length);
      $(this).parent().children('span').text('Imagen cargada');
      $("#filename").html(' <i class="fa fa-file-image-o"></i> ' + $imgName);
    });
  </script>
@endsection