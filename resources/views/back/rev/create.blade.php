<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="revisionesModalLabel">Nuevo cambio</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
      <div class="modal-body text-left">
      {!! Form::open(['route' => 'revisiones.store', 'class' => 'col-12']) !!}
        @if($table == "dev")
          {{ Form::hidden('task_id', $t->id) }}
        @elseif($table == "sys")
          {{ Form::hidden('sys_task_id', $t->id) }}
        @endif
        {{ Form::hidden('autor_id', Auth::user()->id) }}
        <div class="form-inline col-12">
          <div class="form-group">
            {{ Form::label('num_rev', 'Numero de revisión') }}
            {{ Form::number('num_rev', $r, ['class' => 'form-control col-12']) }}
          </div>
          @if($table == "dev")
            <div class="form-group left-spacing-50">
              {{ Form::label('hash', 'Hash') }}
              {{ Form::text('hash', null, ['class' => 'form-control col-12']) }}
            </div>
          @endif
        </div>
        <div class="form-group col-12">
          {{ Form::label('desc','Descripcion') }}
          {{ Form::textarea('desc', null,['class' => 'form-control']) }}
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
        {{ Form::submit('Guardar', ['class' => 'btn btn-outline-dark btn-block']) }}  
      </div>
      {{ Form::close() }}
    </div>
  </div>
</div>