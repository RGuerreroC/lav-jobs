@extends('_templates.main')

@section('title', 'Cambios')

@section('stylesheets')
    {{ Html::style(asset('css/revisiones/index.css')) }}   
@endsection

@section('jumbotron')
  <div class="jumbotron jumbotron top-spacing-20">
    <div class="container">
      <h1 class="display-5 text-center">
       Ver cambio
      </h1>
      <hr />
      <p class="lead row justify-content-between">
        <a href="{{ URL::previous() }}" class="btn btn-secondary col-1"><span class="fa fa-arrow-left fa-lg"></span></a>
      </p>
    </div>
  </div>
@endsection

@section('content')
<div class="col-12 row justify-content-between">
  <div class="col-10 row justify-content-between">
    <div class="col-12">
      <div class="card">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title"><span class="btn btn-secondary circle fa fa-info"></span> Revisión {{ $rev->num_rev }}</h4>
            <h6 class="card-subtitle mb-2 text-muted">{{ $rev->hash === null ? 'Sin hash' : 'Hash: '.$rev->hash }}</h6>
          </div>
          <div class="card-body">
            <div class="card-text">
              {!! $rev->desc !!}
            </div>
          </div>
          <div class="card-footer">
            
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col ml-auto">
    <div class="card">
      <div class="card-body">
        <h5>Información</h5>
        <ul class="nav flex-column text-left">
          <li class="nav-item">
            <span class="fa fa-calendar"></span> Creado
          </li>
          <li class="nav-item">
            {{ Date::parse($rev->created_at)->format('d/m/Y') }}
            {{ Date::parse($rev->created_at)->format('H:i') }}
          </li>
          <hr />
          <li class="nav-item">
            <span class="fa fa-calendar"></span> Modificado
          </li>
          <li class="nav-item">
            {{ Date::parse($rev->updated_at)->format('d/m/Y') }}
            {{ Date::parse($rev->updated_at)->format('H:i') }}
          </li>
          <li class="nav-item">
            <span class="fa fa-user"></span> Por:
          </li>
          <li class="nav-item">
            {{ $rev->user->name }}
          </li>
        </ul>
      </div>
      <a href="{{ route('revisiones.edit', $rev->id) }}" class="btn btn-outline-primary btn-block"><span class="fa fa-pencil"></span> Editar</a>
    </div>
      
  </div>
</div>
@endsection