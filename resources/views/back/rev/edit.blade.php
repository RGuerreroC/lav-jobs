@extends('_templates.main')

@section('title', 'Cambios')

@section('stylesheets')
    {{ Html::style(asset('css/tareas/index.css')) }}
    {{ Html::style('http://cdn.dxcodercrew.net/select2/dist/css/select2.css') }}
    <script src="http://cdn.dxcodercrew.net/tinymce/tinymce.min.js"></script>

    <script type="text/javascript">
    tinymce.init({
        selector: 'textarea'
        ,language: 'es'
        ,plugins: [
            "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak code"
        ]
        ,height: 200
        ,max_height: 200
        ,min_height: 200
        ,branding: false
    });
    </script>
@endsection

@section('jumbotron')
  <div class="jumbotron jumbotron top-spacing-20">
    <div class="container">
      <h1 class="display-5 text-center">
       Editar cambio
      </h1>
      <hr />
      <p class="lead row justify-content-between">
        <a href="{{ URL::previous() }}" class="btn btn-secondary col-1"><span class="fa fa-arrow-left fa-lg"></span></a>
      </p>
    </div>
  </div>
@endsection

@section('content')
<div class="col-12 row justify-content-between">
  <div class="col-10 row justify-content-start">
    {{ Form::model($rev, ['route' =>['revisiones.update', $rev->id],'files' => 'false', 'method' => 'PUT', 'class' => 'col-12']) }}
      {{ Form::hidden('task_id', $rev->task_id) }}
      {{ Form::hidden('autor_id', Auth::user()->id) }}
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">
            <span class="btn btn-success circle fa fa-pencil"></span>

          </h4>
          <h6 class="card-subtitle mb-2 text-muted"></h6>
        </div>
        <div class="card-body">
          <div class="card-text row">
            <div class="form-group col-4">
              {{ Form::label('num_rev', 'Numero de revisión') }}
              {{ Form::number('num_rev', null, ['class' => 'form-control']) }}  
            </div>
            <div class="form-group col-4">
              {{ Form::label('hash', 'Hash') }}
              {{ Form::text('hash', null, ['class' => 'form-control']) }}  
            </div>
            <div class="form-group col-12">
              {{ Form::label('desc','Descripcion') }}
              {{ Form::textarea('desc', null,['class' => 'form-control']) }}
            </div>
          </div>
        </div>
        <div class="card-footer">
          {{ Form::submit('Guardar', ['class' => 'btn btn-outline-dark btn-block']) }}
        </div>
      </div>
    {{ Form::close() }}
  </div>
  <div class="col ml-auto">
    <div class="card">
      <div class="card-body">
        <h5>Información</h5>
        <ul class="nav flex-column text-left">
          <li class="nav-item">
            <span class="fa fa-calendar"></span> Creado
          </li>
          <li class="nav-item">
            {{ Date::parse($rev->created_at)->format('d/m/Y') }}
            {{ Date::parse($rev->created_at)->format('H:i') }}
          </li>
          <hr />
          <li class="nav-item">
            <span class="fa fa-calendar"></span> Modificado
          </li>
          <li class="nav-item">
            {{ Date::parse($rev->updated_at)->format('d/m/Y') }}
            {{ Date::parse($rev->updated_at)->format('H:i') }}
          </li>
          <li class="nav-item">
            <span class="fa fa-user"></span> Por:
          </li>
          <li class="nav-item">
            {{ $rev->user->name }}
          </li>
        </ul>
      </div>
      {{ Form::open(['route' => ['revisiones.destroy', $rev->id], 'method' => 'DELETE']) }}
        {{ Form::submit('Eliminar', ['class' => 'btn btn-outline-danger btn-block']) }}
      {{ Form::close() }}
    </div>
      
  </div>
</div>
@endsection