@extends('_templates.main')

@section('title', 'Login')

@section('content')
    <div class="container top-spacing-50">
        <div class="row justify-content-center">
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        {{ Form::open() }}
                        {{ csrf_field() }}
                        <div class="form-group">
                            {{ Form::label('username', 'Usuario') }}
                            {{ Form::text('username', null, ['class' => 'form-control form-control-sm']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('password', 'Contraseña') }}
                            {{ Form::password('password', ['class' => 'form-control form-control-sm']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::submit('Login', ['class' => 'btn btn-outline-secondary btn-block']) }}
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
