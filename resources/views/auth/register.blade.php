@extends('_templates.main')

@section('title', 'Registro')

@section('content')
<div class="mdl-grid">
  <div class="mdl-cell mdl-cell--12-col">
    <div class="mdl-cell--4-col mdl-cell--6-offset">
      {{ Form::open() }}
        {{ csrf_field() }}
      <div class="mdl-cell--12-col mdl-card mdl-shadow--2dp">
        <div class="mdl-card__title">
          <h2 class="mdl-card__title-text">Registrar</h2>
        </div>
        <div class="mdl-card__supporting-text">
          <div class="mdl-cell--5-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
            {{ Form::label('name', 'Nombre') }}
            {{ Form::text('name', null, ['class' => 'form-control form-control-sm']) }}
          </div>
          <div class="mdl-cell--5-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
            {{ Form::label('username', 'Usuario') }}
            {{ Form::text('username', null, ['class' => 'form-control form-control-sm']) }}
          </div>
          <div class="mdl-cell--5-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
            {{ Form::label('email', 'Email') }}
            {{ Form::email('email', null, ['class' => 'form-control form-control-sm']) }}
          </div>
          <div class="mdl-cell--5-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
            {{ Form::label('password', 'Contraseña') }}
            {{ Form::password('password', ['class' => 'form-control form-control-sm']) }}
          </div>
          <div class="mdl-cell--5-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
            {{ Form::label('password_confirmation', 'Repetir contraseña') }}
            {{ Form::password('password_confirmation',['class' => 'form-control form-control-sm']) }}
          </div>
        </div>
        <div class="mdl-card__actions mdl-card--border">
          {{ Form::submit('Registrar', ['class' => 'btn btn-dark btn-block']) }}
        </div>
      </div>
        {{ Form::hidden('confirmation_code', str_random(30)) }}
      {{ Form::close() }}
    </div>
  </div>
</div>
@endsection
