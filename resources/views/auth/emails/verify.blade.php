<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Verificar email</h2>

        <div>
            Gracias por registrarte en la intranet.
            Por favor sigue el enlace siguiente para verificar y activar la cuenta:
            {{ URL::to('registrar/verify/' . $confirmation_code) }}.<br/>
        </div>

    </body>
</html>
