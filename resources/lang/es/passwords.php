<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'La contraseña debe tener al menos seis caracteres y deben coincidir ambos campos.',
    'reset' => 'Tu contraseña se ha reseteado!',
    'sent' => 'Te hemos enviado un e-mail con el enlace para resetear la contraseña.',
    'token' => 'El token para resetear la contraseña no es correcto.',
    'user' => "No hemos encontrado el e-mail proporcionado en la base de datos.",

];
