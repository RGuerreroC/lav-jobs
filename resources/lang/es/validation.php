<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute debe ser aceptado.',
    'active_url'           => ':attribute no es una URL valida.',
    'after'                => ':attribute debe ser una fecha posterior a :date.',
    'after_or_equal'       => ':attribute debe ser una fecha posterior o igual a :date.',
    'alpha'                => ':attribute solo debe contener letras.',
    'alpha_dash'           => ':attribute solo debe contener letras, numeros y guiones.',
    'alpha_num'            => ':attribute solo debe contener letras y numeros.',
    'array'                => ':attribute debe ser un array.',
    'before'               => ':attribute debe ser una fecha anterior a :date.',
    'before_or_equal'      => ':attribute debe ser una fecha anterior o igual a :date.',
    'between'              => [
        'numeric' => ':attribute debe estar entre :min y :max.',
        'file'    => ':attribute debe estar entre :min y :max kb.',
        'string'  => ':attribute debe estar entre :min y :max caracteres.',
        'array'   => ':attribute debe tener entre :min y :max elementos.',
    ],
    'boolean'              => ':attribute debe ser verdadero o falso.',
    'confirmed'            => ':attribute no coincide la confirmación.',
    'date'                 => ':attribute no es una fecha valida.',
    'date_format'          => ':attribute no coincide con el formato :format.',
    'different'            => ':attribute y :other deben ser diferentes.',
    'digits'               => ':attribute debe tener :digits como digitos.',
    'digits_between'       => ':attribute debe estar entre :min y :max digitos.',
    'dimensions'           => ':attribute tiene dimensiones incorrectas.',
    'distinct'             => ':attribute tiene valores duplicados.',
    'email'                => ':attribute debe ser un e-mail valido.',
    'exists'               => ':attribute seleccionado no existe.',
    'file'                 => ':attribute debe ser un archivo.',
    'filled'               => ':attribute debe ser un valor.',
    'image'                => ':attribute debe ser una imagen.',
    'in'                   => ':attribute seleccionado no es valido.',
    'in_array'             => ':attribute no existe en :other.',
    'integer'              => ':attribute debe ser un entero.',
    'ip'                   => ':attribute debe ser una dirección IP valida.',
    'ipv4'                 => ':attribute debe ser una dirección IPv4 valida.',
    'ipv6'                 => ':attribute debe ser una dirección IPv6 valida.',
    'json'                 => ':attribute debe ser una linea JSON valida.',
    'max'                  => [
        'numeric' => ':attribute no debe ser mayor que :max.',
        'file'    => ':attribute no debe ser mayor que :max kb.',
        'string'  => ':attribute no debe ser mayor que :max caracteres.',
        'array'   => ':attribute no debe tener más de :max elementos.',
    ],
    'mimes'                => ':attribute debe ser un archivo tipo: :values.',
    'mimetypes'            => ':attribute debe ser un archivo tipo: :values.',
    'min'                  => [
        'numeric' => ':attribute no debe ser menor que  :min.',
        'file'    => ':attribute no debe ser menor que  :min kb.',
        'string'  => ':attribute no debe ser menor que  :min caracteres.',
        'array'   => ':attribute no debe tener menos de :min elementos.',
    ],
    'not_in'               => ':attribute seleccionado no es valido.',
    'numeric'              => ':attribute debe ser un numero.',
    'present'              => ':attribute tiene que estar presente.',
    'regex'                => ':attribute formato no valido.',
    'required'             => ':attribute es requerido.',
    'required_if'          => ':attribute es requerido cuando :other es :value.',
    'required_unless'      => ':attribute es requerido a no ser que :other este en :values.',
    'required_with'        => ':attribute es requerido cuando los valores :values este presente.',
    'required_with_all'    => ':attribute es requerido cuando los valores :values este presente.',
    'required_without'     => ':attribute es requerido cuando los valores :values no este presente.',
    'required_without_all' => ':attribute es requerido cuando ninguno de los valores :values este presente.',
    'same'                 => ':attribute y :other deben coincidir.',
    'size'                 => [
        'numeric' => ':attribute debe tener :size.',
        'file'    => ':attribute debe tener :size kb.',
        'string'  => ':attribute debe tener :size caracteres.',
        'array'   => ':attribute debe contener :size elementos.',
    ],
    'string'               => ':attribute debe tener una linea.',
    'timezone'             => ':attribute debe tener una zona valida.',
    'unique'               => ':attribute ya existe.',
    'uploaded'             => ':attribute ha fallado la subida.',
    'url'                  => ':attribute no tiene un formato valido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
