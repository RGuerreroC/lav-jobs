<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middlewareGroups' => ['web']], function(){
    // Authentication Routes
    Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
    Route::post('login', ['as' => 'login', 'uses' => 'Auth\LoginController@login']);
    Route::get('logoff', ['as' => 'logoff', 'uses' => 'Auth\LoginController@logout']);

    //                                              Register Routes                                            //
    /***********************************************************************************************************
    Route::get('registrar', ['as' => 'registrar', 'uses' => 'Auth\RegisterController@showRegistrationForm']);
    Route::post('registrar', ['as' => 'registrar', 'uses' => 'Auth\RegisterController@register']);
    Route::get('registrar/verify/{confirmationCode}', [
         'as' => 'confirmation_path'
        ,'uses' => 'Auth\RegisterController@confirm' ]);
    ***********************************************************************************************************/
    // Resources with resources
    Route::resource('recursos', 'ResourcesCtrl', ['except' => ['show', 'create']]);
    // Tasks with resources
    Route::resource('tareas', 'TasksCtrl', ['except' => 'create']);
        // Tasks other
        Route::get('tareas/estado/{estat?}',[ 'as' => 'tareas.estado', 'uses' => 'TasksCtrl@filtrado']); // Filtrado selectivo
        //Route::get('estatUpdate/{tasca}/{estat}', 'TasksCtrl@estatUpdate'); // Updatea solo el estat
        Route::post('estatUpdate', 'TasksCtrl@estatUpdate'); // Updatea solo el estat
        Route::post('titleChangeDev', 'TasksCtrl@titleChange'); // Prueba de ajax
    // Revisions with resources
    Route::resource('revisiones', 'RevisionsCtrl', ['index', 'create']);
    // SysTasks with Resources
    Route::resource('sys_tareas', 'SysTasksCtrl', ['except' => 'create']);
        // SysTasks other
        Route::get('sys_tareas/estado/{estat?}',[ 'as' => 'sys_tareas.estado', 'uses' => 'SysTasksCtrl@filtrado']); // Filtrado selectivo
        Route::post('titleChangeSys', 'SysTasksCtrl@titleChange'); // Prueba de ajax
        Route::post('estatUpdateSys','SysTasksCtrl@estatUpdate');


    // Worked Hours With Resources
    Route::resource('horas', 'WorkedHoursCtrl', ['except' => 'create']);
        Route::get('horas/{month_year?}/index/', ['as' => 'horas.index', 'uses' => 'WorkedHoursCtrl@index']);
        Route::get('horas/{month_year?}/auto/', ['as' => 'horas.auto', 'uses' => 'WorkedHoursCtrl@autoCreate']);
    // Paginas one-page
    Route::get('/', 'PagsCtrl@getIndex');
});
