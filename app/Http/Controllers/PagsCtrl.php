<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ResourceItem;

class PagsCtrl extends Controller
{
    public function getIndex()
    {
      $res = ResourceItem::orderBy('created_at')->get();
      return view('front.index')
              ->withRes($res);
    }
}
