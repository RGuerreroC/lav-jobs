<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use App\ResourceItem;
use Session;
use Image;


class ResourcesCtrl extends Controller
{

  public function __construct()
  {
    $this->res = ResourceItem::orderBy('created_at')->get();
    $this->middleware('auth');
  }
  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  public function index()
  {
    return view('back/res/index')->withRes($this->res);
  }

  /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
  public function create()
  {
    // Not in use
  }

  /*
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request,[
      'nombre' => 'required|max:250',
      'url' => 'required'
    ]);

    $res = new ResourceItem;
    if($request->interno == 'true')
    {
      $res->interno = false;
    }else{
      $res->interno = true;
    }
    $res->id = Uuid::generate()->string;
    $res->nombre = $request->nombre;
    $res->url = $request->url;

    // Guardar imagen
    if($request->hasFile('iconImg'))
    {
      $img_name = strtolower(str_replace(' ', '_', $request->nombre));
      $image = $request->file('iconImg');
      $filename = $img_name . '.' . $image->getClientOriginalExtension();
      $location = public_path('images/'.$filename);

      Image::make($image)->save($location);

      $res->icono = $filename;
    }else{
      $res->icono = 'default';
    }

    $res->save();

    Session::flash('success', 'Recurso guardado correctamente');
    // Redirigir a otra pagina

    return redirect()->route('recursos.index');
  }

  /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function show($id)
  {
    // Not in use
  }

  /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function edit($id)
  {
    $re = ResourceItem::find($id);
    return view('back/res/edit')
      ->withRes($this->res)
      ->withRe($re);
  }

  /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function update(Request $request, $id)
  {
    $this->validate($request,[
      'nombre' => 'required|max:250',
      'url' => 'required'
    ]);

    $res = ResourceItem::find($id);
    if($request->interno == 'true')
    {
      $res->interno = false;
    }else{
      $res->interno = true;
    }

    $old_name = $res->nombre;
    $res->id = Uuid::generate()->string;
    $res->nombre = $request->nombre;
    $res->url = $request->url;
    $extension = substr($res->icono, strripos($res->icono, '.'));

    if($old_name != $request->nombre && $res->icono != 'default')
    {
      $this->delMove(['old' => $old_name, 'new' => $request->nombre, 'type' => $extension],'mv');
      $old_name = $request->nombre;
    }

    // Guardar imagen
    if($request->hasFile('iconImg'))
    {
      if(File::exists(public_path('images/'.$res->icono))){
        $this->delMove(['old' => $old_name],'del');
      }
      $img_name = strtolower(str_replace(' ', '_', $request->nombre));
      $image = $request->file('iconImg');
      $filename = $img_name . '.' . $image->getClientOriginalExtension();
      $location = public_path('images/'.$filename);

      Image::make($image)->save($location);

      $res->icono = $filename;
    }else{
      if($request->del_img == 'true')
      {
        $this->delMove(['old' => $old_name, 'type' => $extension],'del');
        $res->icono = 'default';
      }
    }

    $res->save();

    Session::flash('success', 'Recurso guardado correctamente');
    // Redirigir a otra pagina

    return redirect()->route('recursos.index');
  }

  /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function destroy($id)
  {
    $res = ResourceItem::find($id);
    if(File::exists(public_path('images/'.$res->icono))){
      $this->delMove(['old' => $res->nombre],'del');
    }
    $res->delete();

    Session::flash('success', 'Recurso Borrado');
    // Redirigir a otra pagina
    return redirect()->route('recursos.index');

  }

  private function delMove($paths,$action)
  {
    switch ($action)
    {
      case 'mv':
        $img_name_old = strtolower(str_replace(' ', '_', $paths['old']));
        $img_name_new = strtolower(str_replace(' ', '_', $paths['new']));
        File::move(public_path('images/'.$img_name_old.$paths['type']), public_path('images/'.$img_name_new.$paths['type']));
        break;
      case 'del':
        $img_name_old = strtolower(str_replace(' ', '_', $paths['old']));
        File::delete(public_path('images/'.$img_name_old.$paths['type']));
        break;
    }
  }
}
