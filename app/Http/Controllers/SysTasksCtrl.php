<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use App\ResourceItem;
use App\SysTask;
use Session;
use Response;

class SysTasksCtrl extends Controller
{
  public function __construct()
  {
    $this->res = ResourceItem::orderBy('created_at')->get();
    $this->middleware('auth');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ts = SysTask::orderBy('estat', 'ASC')->orderBy('glpi_id', 'asc')->paginate(10);
        $tAll = SysTask::count();
        $tNew = SysTask::where('estat','=',0)->count();
        $tCur = SysTask::where('estat','=',1)->count();
        $tOk = SysTask::where('estat','=',2)->count();
        $tKo = SysTask::where('estat','=',3)->count();
        return view('back/sys/index')
          ->withRes($this->res)
          ->withTs($ts)
          ->withTAll($tAll)
          ->withTNew($tNew)
          ->withTCur($tCur)
          ->withTOk($tOk)
          ->withTKo($tKo);
    }

  public function filtrado($filtre)
  {
    $tAll = SysTask::count();
    $tNew = SysTask::where('estat','=',0)->count();
    $tCur = SysTask::where('estat','=',1)->count();
    $tOk = SysTask::where('estat','=',2)->count();
    $tKo = SysTask::where('estat','=',3)->count();
    if($filtre === null){
      $ts = SysTask::orderBy('estat', 'ASC')->orderBy('glpi_id', 'asc')->paginate(10);
    }else{
      $ts = SysTask::where('estat', '=', $filtre)->orderBy('glpi_id', 'asc')->paginate(10);
    }

    return view('back/sys/index')
      ->withRes($this->res)
      ->withTs($ts)
      ->withTAll($tAll)
      ->withTNew($tNew)
      ->withTCur($tCur)
      ->withTOk($tOk)
      ->withTKo($tKo);
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Not in use
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $ts = new SysTask;

      $ts->id = Uuid::generate()->string;
      $ts->project_name = $request->project_name;
      $ts->desc = $request->desc;
      $ts->start_date = $request->start_date;
      $ts->finish_date = $request->finish_date;
      $ts->glpi_id = $request->glpi_id;
      $ts->estat = 0;

      if($ts->save()){
        Session::flash('success', 'Tarea creada correctamente');
      }else{
        Session::flash('error', 'Tarea no creada');
      }

      return redirect()->route('sys_tareas.estado', '0');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {//
      $t = SysTask::find($id);
      $table = "sys";

      return view('back/sys/show')
          ->withRes($this->res)
          ->withT($t)
          ->withTable($table);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $t = SysTask::find($id);

      return view('back/sys/edit')
          ->withRes($this->res)
          ->withT($t);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $ts = SysTask::find($id);


      $ts->project_name = $request->project_name;
      $ts->id = Uuid::generate()->string;
      $ts->project_name = $request->project_name;
      $ts->desc = $request->desc;
      $ts->start_date = $request->start_date;
      $ts->finish_date = $request->finish_date;
      $ts->glpi_id = $request->glpi_id;

      if($ts->save()){
        Session::flash('success', 'Tarea creada correctamente');
      }else{
        Session::flash('error', 'Tarea no creada');
      }

      return redirect()->route('sys_tareas.show', $ts->id);
    }

    public function titleChange(Request $request){
      $ts = SysTask::find($request->tarea);

      $ts->project_name = $request->projectName;

      if($ts->save()){
        Session::flash('success', 'Tarea modificada correctamente');
      }else{
        Session::flash('error', 'Tarea no modificada');
      }
    }

    public function estatUpdate(Request $request){
      $ts = SysTask::find($request->tasca);
      $ts->estat = $request->estat;

      if($ts->save()){
        Session::flash('success', 'Tarea modificada correctamente');
      }else{
        Session::flash('error', 'Tarea no modificada');
      }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ts = SysTask::find($id);
        if($ts->delete()){
          Session::flash('success', 'Tarea eliminada correctamente');
        }else{
          Session::flash('error', 'Tarea no eliminada');
        }

        return redirect()->route('sys_tareas.index');
    }
}
