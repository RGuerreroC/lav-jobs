<?php

namespace App\Http\Controllers\Auth;

use Mail;
use Session;
use App\User;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'id' => Uuid::generate(),
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'confirmation_code' => $data['confirmation_code'],
            'password' => bcrypt($data['password']),
            'admin' => false
        ]);
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $data = array(
            'email' => $request->email
            ,'name' =>  $request->name
            ,'confirmation_code' => $request->confirmation_code
        );

        Mail::send('auth.emails.verify', $data, function($message) use ($data){
            $message->from('no.reply@dxcodercrew.net', 'DX CoderCrew')
            ->to($data['email'])
            ->subject('Verificación de email para :: ' . $data['name']);
        });

        Session::flash('success', 'Gracias por registrarte. Por favor, revisa tu email');
        return redirect()->action('PagsController@getIndex');
    }

    public function confirm($confirmation_code)
    {
        if( ! $confirmation_code)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user = User::whereConfirmationCode($confirmation_code)->first();

        if ( ! $user)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user->confirmed = 1;
        $user->confirmation_code = null;
        $user->save();

        Session::flash('success', 'La cuenta se ha verificado correctamente');

        return redirect()->route('login');
    }
}
