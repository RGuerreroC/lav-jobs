<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use App\ResourceItem;
use App\Task;
use Session;
use Revision;
use Response;

class TasksCtrl extends Controller
{
  public function __construct()
  {
    $this->res = ResourceItem::orderBy('created_at')->get();
    $this->middleware('auth');
  }
  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  public function index()
  {
    $ts = Task::orderBy('estat', 'ASC')->orderBy('redmine_id', 'asc')->paginate(10);
    $tAll = Task::count();
    $tNew = Task::where('estat','=',0)->count();
    $tCur = Task::where('estat','=',1)->count();
    $tOk = Task::where('estat','=',2)->count();
    $tKo = Task::where('estat','=',3)->count();
    return view('back/tareas/index')
      ->withRes($this->res)
      ->withTs($ts)
      ->withTAll($tAll)
      ->withTNew($tNew)
      ->withTCur($tCur)
      ->withTOk($tOk)
      ->withTKo($tKo);
  }

  public function filtrado($filtre)
  {
    $tAll = Task::count();
    $tNew = Task::where('estat','=',0)->count();
    $tCur = Task::where('estat','=',1)->count();
    $tOk = Task::where('estat','=',2)->count();
    $tKo = Task::where('estat','=',3)->count();
    if($filtre === null){
      $ts = Task::orderBy('estat', 'ASC')->orderBy('redmine_id', 'asc')->paginate(10);
    }else{
      $ts = Task::where('estat', '=', $filtre)->orderBy('redmine_id', 'asc')->paginate(10);
    }

    return view('back/tareas/index')
      ->withRes($this->res)
      ->withTs($ts)
      ->withTAll($tAll)
      ->withTNew($tNew)
      ->withTCur($tCur)
      ->withTOk($tOk)
      ->withTKo($tKo);
  }

  /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
  public function create()
  {
    // Not in use
  }

  /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
  public function store(Request $request)
  {
    $ts = new Task;

    $ts->id = Uuid::generate()->string;
    $ts->project_name = $request->project_name;
    $ts->glpi_id = $request->glpi_id;
    $ts->redmine_id = $request->redmine_id;
    $ts->redmine_t = $request->redmine_t;
    $ts->repo = $request->repo;
    $ts->desc = $request->desc;
    $ts->estat = 0;

    if($ts->save()){
      Session::flash('success', 'Tarea creada correctamente');
    }else{
      Session::flash('error', 'Tarea no creada');
    }

    return redirect()->route('tareas.estado', '0');
  }

  /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function show($id)
  {
    $t = Task::find($id);
    $table = "dev";
    $tmp = $t->part_rev($id)->first();

    $r = $tmp != null ? $tmp->num_rev+1 : 1;

    return view('back/tareas/show')
      ->withRes($this->res)
      ->withT($t)
      ->withR($r)
      ->withTable($table);
  }

  /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function edit($id)
  {
    $t = Task::find($id);

    return view('back/tareas/edit')
      ->withRes($this->res)
      ->withT($t);
  }

  /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function update(Request $request, $id)
  {
    $ts = Task::find($id);

    $ts->project_name = $request->project_name;
    $ts->glpi_id = $request->glpi_id;
    $ts->redmine_id = $request->redmine_id;
    $ts->redmine_t = $request->redmine_t;
    $ts->repo = $request->repo;
    $ts->desc = $request->desc;
    $ts->estat = $request->estat;

    if($ts->save()){
      Session::flash('success', 'Tarea modificada correctamente');
    }else{
      Session::flash('error', 'Tarea no modificada');
    }

    return redirect()->route('tareas.show', $ts->id);
  }
  /**
   * Update estat in storage.
   *
   * @param  string  $tasca
   * @param  string  $estat
   * @return \Illuminate\Http\Response
   */
  public function estatUpdate(Request $request){
    $ts = Task::find($request->tasca);
    $ts->estat = $request->estat;

    if($ts->save()){
      Session::flash('success', 'Tarea modificada correctamente');
    }else{
      Session::flash('error', 'Tarea no modificada');
    }
  }

  public function titleChange(Request $request){
    $ts = Task::find($request->tarea);

    $ts->project_name = $request->projectName;

    if($ts->save()){
      Session::flash('success', 'Tarea modificada correctamente');
    }else{
      Session::flash('error', 'Tarea no modificada');
    }
  }
  /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function destroy($id)
  {
    $ts = Task::find($id);
    if($ts->delete()){
      Session::flash('success', 'Tarea eliminada correctamente');
    }else{
      Session::flash('error', 'Tarea no eliminada');
    }

    return redirect()->route('tareas.index');
  }

  /**
  * Prueba de ajax
  *
  *
  */


}
