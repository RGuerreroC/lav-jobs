<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use App\ResourceItem;
use App\Revision;
use Session;


class RevisionsCtrl extends Controller
{
  public function __construct()
  {
    $this->res = ResourceItem::orderBy('created_at')->get();
    $this->middleware('auth');
  }
  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  public function index()
  {
    // Not in use
  }

  /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
  public function create()
  {
    // Not in use
  }

  /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
  public function store(Request $request)
  {
    $this->validate($request,[
      'num_rev' => 'required'
    ]);

    $rev = new Revision();
    $rev->id = Uuid::generate()->string;
    if(isset($request->task_id)){
      $rev->task_id = $request->task_id;
    }elseif(isset($request->sys_task_id)){
      $rev->sys_task_id = $request->sys_task_id;
    }
    $rev->num_rev = $request->num_rev;
    $rev->hash = $request->hash;
    $rev->autor_id = $request->autor_id;
    $rev->desc = $request->desc;

    if($rev->save()){
      Session::flash('success', 'Cambio creado correctamente');
    }else{
      Session::flash('error', 'Cambio no creado');
    }

    return redirect()->route('tareas.show', $request->task_id);
  }

  /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function show($id)
  {
    $rev = Revision::find($id);

    return view('back/rev/show')
      ->withRes($this->res)
      ->withRev($rev);
  }

  /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function edit($id)
  {
    $rev = Revision::find($id);

    return view('back/rev/edit')
      ->withRes($this->res)
      ->withRev($rev);
  }

  /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function update(Request $request, $id)
  {
    $this->validate($request,[
      'num_rev' => 'required'
    ]);

    $rev = Revision::find($id);

    $rev->task_id = $request->task_id;
    $rev->num_rev = $request->num_rev;
    $rev->hash = $request->hash;
    $rev->autor_id = $request->autor_id;
    $rev->desc = $request->desc;

    if($rev->save()){
      Session::flash('success', 'Cambio modificado correctamente');
    }else{
      Session::flash('error', 'Cambio no modificado');
    }

    return redirect()->route('revisiones.show', $id);
  }

  /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function destroy($id)
  {
    $rev = Revision::find($id);
    $task = $rev->task_id;

    if( $rev->delete()){
      Session::flash('success', 'Cambio eliminado correctamente');
    }else{
      Session::flash('error', 'Cambio no eliminado');
    }

    return redirect()->route('tareas.show', $task);
  }
}
