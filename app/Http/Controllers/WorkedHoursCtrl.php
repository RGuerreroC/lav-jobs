<?php

namespace App\Http\Controllers;

use App\Exports\InputsExport;
use Illuminate\Http\Request;
use Jenssegers\Date\Date;
use Webpatser\Uuid\Uuid;
use App\ResourceItem;
use App\WorkedHour;
use Response;
use Session;
use Excel;

class WorkedHoursCtrl extends Controller
{
    public function __construct()
    {
        $this->res = ResourceItem::orderBy('created_at')->get();
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($month_year="current")
    {
        if($month_year == "current"){
            $date = Date::now()->format('F Y');
            $dateS = Date::createFromFormat('Y-m-d', Date::now()->startOfMonth()->format("Y-m-d"));
            $dateS->hour = 0;
            $dateS->minute = 0;
            $dateS->second = 0;
            $dateE = Date::createFromFormat('Y-m-d', Date::now()->endOfMonth()->format("Y-m-d"));
            $dateE->hour = 23;
            $dateE->minute = 59;
            $dateE->second = 59;
        }else{
            $date_expl = explode('-', $month_year);
            $date = Date::createFromDate($date_expl[1], $date_expl[0], '01')->format('F Y');
            $dateS = Date::createFromDate($date_expl[1], $date_expl[0], '01')->format("Y-m-d 00:00:00");
            $dateE = Date::createFromDate($date_expl[1], $date_expl[0], '01')->endOfMonth()->format("Y-m-d 23:59:59");
        }

        $arrayDate = explode(' ', $date);
        $month = $arrayDate[0];
        $year = $arrayDate[1];
        $whs = WorkedHour::whereBetween('start_date', [$dateS,$dateE])
            ->orderBy('start_date', 'asc')->get();

        return view('back/horas/index')
            ->withRes($this->res)
            ->withDate($date)
            ->withMonth($month)
            ->withYear($year)
            ->withWhs($whs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function autoCreate($month_year){
      $error = 0;
      //$month_year = '10-2018';
      $date_expl = explode('-', $month_year);
      $date = Date::createFromDate($date_expl[1], $date_expl[0], '01')->format('F Y');
      $dateM = Date::createFromDate($date_expl[1], $date_expl[0], '01')->format('F');
      $dateS = Date::createFromDate($date_expl[1], $date_expl[0], '01')->format("Y-m-d 00:00:00");
      $dateE = Date::createFromDate($date_expl[1], $date_expl[0], '01')->endOfMonth()->format("Y-m-d 23:59:59");

      $whs = WorkedHour::whereBetween('start_date', [$dateS,$dateE])
          ->orderBy('start_date', 'desc')->first();
      if($whs != null){
        $weekday = Date::parse($whs->start_date)->add('1 day')->format("l");
        $month = Date::parse($whs->start_date)->add('1 day')->format("F");
        if($month == $dateM){
          if($weekday != 'domingo' and $weekday != 'sábado'){
            $f_hour = $month == 'agosto' ? '15:00:00' : ($weekday == 'viernes' ? '14:00:00' : '17:00:00');
            $s_hour = '08:00:00';
            $dinner = ($month == 'agosto' or $weekday == 'viernes') ? Date::parse($whs->start_date)->format("00:00:00") : Date::parse($whs->start_date)->format("00:30:00");
          }else{
            $f_hour = '00:00:00';
            $s_hour = '00:00:00';
            $dinner = '00:00:00';
          }
          $start_date = Date::parse($whs->start_date)->add('1 day')->format("Y-m-d ".$s_hour);
          $finish_date = Date::parse($whs->start_date)->add('1 day')->format("Y-m-d ".$f_hour);
        }else{
          $error = 1;
        }
      }else{
        $weekday = Date::parse('01-'.$month_year)->format("l");
        $month = Date::parse('01-'.$month_year)->format("F");
        if($weekday != 'domingo' and $weekday != 'sábado'){
          $f_hour = $month == 'agosto' ? '15:00:00' : ($weekday == 'viernes' ? '14:00:00' : '17:00:00');
          $s_hour = '08:00:00';
          $dinner = ($month == 'agosto' or $weekday == 'viernes') ? Date::parse('01-'.$month_year)->format("00:00:00") : Date::parse($whs->start_date)->format("00:30:00");
        }else{
          $f_hour = '00:00:00';
          $s_hour = '00:00:00';
          $dinner = '00:00:00';
        }
        $start_date = Date::parse('01-'.$month_year)->format("Y-m-d ".$s_hour);
        $finish_date = Date::parse('01-'.$month_year)->format("Y-m-d ".$f_hour);
      }

      if($error == 0){
        $total_hours = explode(":",$this->get_total_hours($start_date, $finish_date, $dinner));
        $hours_dec = $total_hours[0] + floor(($total_hours[1]/60)*100) / 100;
        $freeD = 0;

        $wh = new WorkedHour;

        $wh->id = Uuid::generate()->string;
        $wh->start_date = $start_date;
        $wh->free_day = $freeD;
        $wh->finish_date = $finish_date;
        $wh->dinner = $dinner;
        $wh->total_hours = $hours_dec;
        $wh->descripcion = "";

        $to_month = Date::parse($start_date)->format('m-Y');

        if($wh->save()){
            Session::flash('success', 'Imputación añadida correctamente');
        }else{
            Session::flash('error', 'Imputación no añadida');
        }

        return redirect()->route('horas.index', $to_month);
      }else{
        $to_month = Date::parse($date)->format('m-Y');
        Session::flash('error', 'Este mes no tiene más dias');
        return redirect()->route('horas.index', $to_month);
      }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'start_date' => 'required',
            'finish_date' => 'required',
            'dinner' => 'required'
        ]);


        if($request->free_day != 'on'){
            echo $request->free_day . " OFF";
            $total_hours = explode(":",$this->get_total_hours($request->start_date, $request->finish_date, $request->dinner));
            $hours_dec = $total_hours[0] + floor(($total_hours[1]/60)*100) / 100;
            $freeD = 0;
        }else{
            echo $request->free_day . " ON";
            $hours_dec = 0;
            $freeD = 1;
        }

        $wh = new WorkedHour;

        $wh->id = Uuid::generate()->string;
        $wh->start_date = $request->start_date;
        $wh->free_day = $freeD;
        $wh->finish_date = $request->finish_date;
        $wh->dinner = $request->dinner;
        $wh->total_hours = $hours_dec;
        $wh->descripcion = $request->descripcion;

        $to_month = Date::createFromFormat('Y-m-d\TH:i', $request->start_date)->format('m-Y');

        if($wh->save()){
            Session::flash('success', 'Imputación añadida correctamente');
        }else{
            Session::flash('error', 'Imputación no añadida');
        }

        return redirect()->route('horas.index', $to_month);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($month_year = "current")
    {
        if($month_year == "current"){
            $date = Date::now()->format('F Y');
            $dateS = Date::createFromFormat('Y-m-d', Date::now()->startOfMonth()->format("Y-m-d"));
            $dateS->hour = 0;
            $dateS->minute = 0;
            $dateS->second = 0;
            $dateE = Date::createFromFormat('Y-m-d', Date::now()->endOfMonth()->format("Y-m-d"));
            $dateE->hour = 23;
            $dateE->minute = 59;
            $dateE->second = 59;
        }else{
            $date_expl = explode('-', $month_year);
            $date = Date::createFromDate($date_expl[1], $date_expl[0], '01')->format('F Y');
            $dateS = Date::createFromDate($date_expl[1], $date_expl[0], '01')->format("Y-m-d 00:00:00");
            $dateE = Date::createFromDate($date_expl[1], $date_expl[0], '01')->endOfMonth()->format("Y-m-d 23:59:59");
        }

        $arrayDate = explode(' ', $date);
        $month = $arrayDate[0];
        $year = $arrayDate[1];
        $whs = WorkedHour::whereBetween('start_date', [$dateS,$dateE])
            ->orderBy('start_date', 'asc')->get();

        return view('back/horas/show')
            ->withRes($this->res)
            ->withMonth($month)
            ->withYear($year)
            ->withWhs($whs);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $wh = WorkedHour::find($id);

        return view('back/horas/edit')
            ->withRes($this->res)
            ->withWh($wh);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'start_date' => 'required',
            'finish_date' => 'required',
            'dinner' => 'required'
        ]);


        if($request->free_day != 'on'){
            echo $request->free_day . " OFF";
            $total_hours = explode(":",$this->get_total_hours($request->start_date, $request->finish_date, $request->dinner));
            $hours_dec = $total_hours[0] + floor(($total_hours[1]/60)*100) / 100;
            $freeD = 0;
        }else{
            echo $request->free_day . " ON";
            $hours_dec = 0;
            $freeD = 1;
        }

        $wh = WorkedHour::find($id);

        $wh->start_date = $request->start_date;
        $wh->free_day = $freeD;
        $wh->finish_date = $request->finish_date;
        $wh->dinner = $request->dinner;
        $wh->total_hours = $hours_dec;
        $wh->descripcion = $request->descripcion;

        $to_month = Date::createFromFormat('Y-m-d\TH:i', $request->start_date)->format('m-Y');

        if($wh->save()){
            Session::flash('success', 'Imputación modificada correctamente');
        }else{
            Session::flash('error', 'Imputación no modificada');
        }

        return redirect()->route('horas.index', $to_month);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $wh = WorkedHour::find($id);
        $to_month = Date::parse($wh->start_date)->format('m-Y');
        if($wh->delete()){
          Session::flash('success', 'Imputación eliminada correctamente');
        }else{
          Session::flash('error', 'Imputación no eliminada');
        }

        return redirect()->route('horas.index', $to_month);
    }

    // Extra Functions
    /**
     * Return total hours
     *
     * @param dateTime $start_date
     * @param dateTime $finish_date
     * @param time $dinner
     * @return float
     */
    private function get_total_hours($start_date, $finish_date, $dinner)
    {
        $st_date = Date::parse($start_date);
        $fi_date = Date::parse($finish_date);
        $di = explode(":",$dinner);
        $di_time = $this->lz($di[0]).":".$this->lz($di[1]).":00";
        $total_hours = $st_date->diffInMinutes($fi_date)/60;

        $seconds = $total_hours*3600;
        $hours = floor($total_hours);
        $seconds -= $hours * 3600;
        $minutes = floor($seconds / 60);
        $hours = $this->lz($hours);
        $minutes = $this->lz($minutes);
        $hours_tot = $this->lz($hours) .":". $this->lz($minutes).":00";
        $final_hours = $this->RestarHoras($di_time, $hours_tot);

        return $final_hours;
    }

    private function lz($num)
    {
        return (strlen($num) < 2) ? "0{$num}" : "{$num}";
    }

    private function RestarHoras($horaini,$horafin)
    {
        $horai=substr($horaini,0,2);
        $mini=substr($horaini,3,2);
        $segi=substr($horaini,6,2);

        $horaf=substr($horafin,0,2);
        $minf=substr($horafin,3,2);
        $segf=substr($horafin,6,2);

        $ini=((($horai*60)*60)+($mini*60)+$segi);
        $fin=((($horaf*60)*60)+($minf*60)+$segf);

        $dif=$fin-$ini;

        $difh=floor($dif/3600);
        $difm=floor(($dif-($difh*3600))/60);
        $difs=$dif-($difm*60)-($difh*3600);
        return date("H:i:s",mktime($difh,$difm,$difs));
    }
}
