<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Task extends Model
{
  protected $primaryKey = "id";
  public $incrementing = false;
  
  public function revision()
  {
    return $this->hasMany('App\Revision');
  }
  
  public function part_rev($tarea_id)
  {
    $result = $this
      ->join('revisions', 'tasks.id','=','revisions.task_id')
      ->select('revisions.id','num_rev','hash')
      ->where('revisions.task_id','=',$tarea_id)
      ->orderBy('num_rev', 'desc')
      ->get();
      
      return $result;
  }
}
