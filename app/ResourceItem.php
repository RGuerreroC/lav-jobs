<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResourceItem extends Model
{
  protected $primaryKey = "id";
  public $incrementing = false;
}
