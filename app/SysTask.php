<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysTask extends Model
{
  protected $primaryKey = "id";
  public $incrementing = false;
  
  public function revision()
  {
    return $this->hasMany('App\Revision');
  }
  
  public function part_rev($tarea_id)
  {
    $result = $this
      ->join('revisions', 'sys_tasks.id','=','revisions.sys_task_id')
      ->select('revisions.id','num_rev','hash')
      ->where('revisions.sys_task_id','=',$tarea_id)
      ->orderBy('num_rev')
      ->get();
      
      return $result;
  }
}
