<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkedHour extends Model
{
    protected $primaryKey = "id";
    public $incrementing = false;
}
