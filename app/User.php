<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'username', 'email', 'password', 'admin', 'confirmation_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $primaryKey = "id";
    public $incrementing = false;
  
    public function revision()
    {
        return $this->hasMany('App\Revision');
    }
/*
    public function lastPost()
    {
        return $this->hasMany('App\Post','autor_id')->where('estado', '=', 1)->orderBy('created_at', 'desc')->first();
    }

    public function profile()
    {
        return $this->hasOne('App\Profile');
    }
  */
}