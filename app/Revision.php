<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Revision extends Model
{
  protected $primaryKey = "id";
  public $incrementing = false;

  public function tasks()
  {
    return $this->belongsTo('App\Task', 'task_id');
  }
  
  public function systasks()
  {
    return $this->belongsTo('App\SysTask', 'sys_task_id');
  }
  
  public function user()
  {
    return $this->belongsTo('App\User', 'autor_id');
  }
}
