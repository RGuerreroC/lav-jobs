<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkedHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worked_hours', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->dateTime('start_date');
            $table->dateTime('finish_date');
            $table->time('dinner');
            $table->float('total_hours', 4, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worked_hours');
    }
}
