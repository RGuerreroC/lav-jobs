<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRevisionsTable extends Migration
{
  /**
     * Run the migrations.
     *
     * @return void
     */
  public function up()
  {
    Schema::create('revisions', function (Blueprint $table) {
      $table->uuid('id');
      $table->primary('id');
      // PK to Task
      $table->uuid('task_id')->nullable();
      $table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade')->onUpdate('cascade');
      
      $table->integer('num_rev');
      $table->string('hash')->nullable();
      // PK to Author
      $table->text('desc')->nullable();
      $table->uuid('autor_id')->nullable();
      $table->foreign('autor_id')->references('id')->on('users')->onDelete('Set null')->onUpdate('cascade');
         
      $table->timestamps();
    });
  }

  /**
     * Reverse the migrations.
     *
     * @return void
     */
  public function down()
  {
    Schema::dropIfExists('revisions');
  }
}
