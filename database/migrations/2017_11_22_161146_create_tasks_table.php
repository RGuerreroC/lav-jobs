<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
  /**
     * Run the migrations.
     *
     * @return void
     */
  public function up()
  {
    Schema::create('tasks', function (Blueprint $table) {
      $table->uuid('id');
      $table->primary('id');
      $table->integer('glpi_id')->nullable();
      $table->integer('redmine_id')->nullable();
      $table->string('redmine_t')->nullable();
      $table->string('repo')->nullable();
      $table->string('desc')->nullable();
      $table->timestamps();
    });
  }

  /**
     * Reverse the migrations.
     *
     * @return void
     */
  public function down()
  {
    Schema::dropIfExists('tasks');
  }
}
