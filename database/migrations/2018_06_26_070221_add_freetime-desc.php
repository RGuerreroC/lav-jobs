<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFreetimeDesc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('worked_hours', function (Blueprint $table) {
            $table->boolean('free_day')->after('dinner')->nullable();
            $table->text('descripcion')->after('free_day')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('worked_hours', function (Blueprint $table) {
            //
        });
    }
}
