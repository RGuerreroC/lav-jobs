<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourceItemsTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('resource_items', function (Blueprint $table) {
      $table->uuid('id');
      $table->primary('id');
      $table->string('nombre');
      $table->boolean('interno');
      $table->string('url');
      $table->string('icono');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('resource_items');
  }
}
